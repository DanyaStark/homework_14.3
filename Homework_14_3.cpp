﻿#include <iostream>
#include <string>
#include <iomanip>

int main()
{
    std::string x1 = "abcdefg";

    std::cout << "Variable: ";
    std::cout << x1 << "\n";

    std::cout << "Number of symbols: ";
    std::cout << x1.length() << "\n";
    
    std::cout << "First symbol: ";
    std::cout << x1.front() << "\n";

    std::cout << "Last symbol: ";
    std::cout << x1.back() << "\n";

}